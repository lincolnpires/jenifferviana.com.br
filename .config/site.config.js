var config = (function () {
  var prodPort = 3000;
  var devPort = 9000;
  var directory = 'dist/';

  return {
    port: prodPort,
    devPort: devPort,
    directory: directory,
    packages: ['../package.json']
  }
})();

module.exports = config;