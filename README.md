# JENIFFER VIANA - ADVOGADA #

This is the repository of the website of Jeniffer Viana, a brasilian lawyer.

* Version: 0.1.0

### How do I get set up? ###

* Clone this repo:
  `git clone git@bitbucket.org:lincolnpires/jenifferviana.com.br.git`
or
  `git clone https://lincolnpires@bitbucket.org/lincolnpires/jenifferviana.com.br.git`
* Install dependencies:
  `npm install`
* Configure the ports on `webpack.config.js`
* Build with and run with `npm start`
* Open your IDE or Editor and start coding

### Deployment instructions ###

* Run `npm prod`
* the `dist` folder is all you need to publish

### Admins? ###

* Lincoln Pires
