const htmlWebpackPlugin = require('html-webpack-plugin');
const extractTextWebpackPlugin = require('extract-text-webpack-plugin');
const copyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');

module.exports = {
	entry: {
		styles: './src/styles.js',
		app: './src/app.js'
	},
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: '[name].[hash].bundle.js'
	},
	module: {
		rules: [{
			test: /\.css$/,
			use: extractTextWebpackPlugin.extract({
				fallback: 'style-loader',
				use: ['css-loader'],
				publicPath: './dist'
			})
		}, {
			test: /\.js$/,
			exclude: /node_modules/,
			use: {
				loader: 'babel-loader'
			}
			// }, {
			// 	test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
			// 	loader: 'url-loader?limit=10000&mimetype=application/font-woff'
			// }, {
			// 	test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
			// 	loader: 'file-loader'
		}]
	},

	plugins: [
		new htmlWebpackPlugin({
			minify: {
				collapseWhitespace: true
			},
			template: './src/index.html'
		}),
		new extractTextWebpackPlugin({
			filename: '[name].[contenthash].bundle.css',
			disable: false,
			allChunks: true
		}),
		new copyWebpackPlugin([{
			from: 'src/favicon.ico'
		}, {
			from: 'src/apple-touch-icon.png'
		}, {
			from: 'src/robots.txt'
		}, {
			from: 'src/404.html'
		}, {
			context: 'src/images',
			from: '**/*',
			to: 'images'
		}, {
			context: 'src/fonts',
			from: '**/*',
			to: 'fonts'
		}, {
			context: 'src/styles',
			from: 'font-awesome.min.css',
			to: 'styles'
		}])
	],

	devServer: {
		contentBase: path.join(__dirname, 'dist'),
		compress: true,
		port: 5000,
		stats: 'errors-only',
		open: false
	},
};
